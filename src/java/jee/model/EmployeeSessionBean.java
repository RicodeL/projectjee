/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jee.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static utils.Constants.*;

/**
 *
 * @author phili
 */
@Stateless
public class EmployeeSessionBean {

    @PersistenceContext(unitName = "JEE_Project_v2_2PU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
   public List<Employees> getEmployees ()
    {
        Query q = em.createQuery("SELECT e FROM Employees e");
        return q.getResultList();
    }
    
   public List<Employees> getEmployeesWithID (String ID)
    {
        Query q = em.createQuery(QUERY_EMP_DETAILS + ID);
        this.em.getEntityManagerFactory().addNamedQuery("selectOneEmployee", q);
        return q.getResultList();
    }
   
   public Employees modifyEmployee (Employees emp)
    {
        
        return em.merge(emp);
    }
   
   public void deleteEmployee(int primaryKey)
   {
        Employees employee = em.find(Employees.class, primaryKey);
        
        em.remove(employee);
   }
   
   
    public List<Credentials> getUsers()
    {
        Query q = em.createQuery("SELECT c FROM Credentials c");
        return q.getResultList();
    }
}
