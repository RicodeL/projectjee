/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author phili
 */
public class Constants {
    public static final String PATH_PROPERTIES_FILE = "utils/db.properties";
    public static final String DB_URL = "dbUrl";
    public static final String DB_USER = "DBuser";
    public static final String DB_PWD = "DBpwd";
    
    
    public static final String WELCOME_PAGE = "WEB-INF/welcome.jsp";
    public static final String LOGIN_PAGE = "WEB-INF/login.jsp";
    public static final String NEWMEMBER = "WEB-INF/newMember.jsp";
    public static final String GOODBYE = "WEB-INF/goodbye.jsp";
    
    
    public static final String NO_MEMBER = "The club has no member";
    public static final String MSG_SELECT_EMPL = "Please select an employee.";
    public static final String WRONG_LOGIN = "Connection failed! Verify your login/password and try again.";
    public static final String FIELDS_EMPTY = "You must enter values in both fields";
    
    
    public static final String INSERT_EMPL = "INSERT INTO EMPLOYEES (\"NAME\", FIRSTNAME, TELHOME, TELMOB, TELPRO, ADRESS, POSTALCODE, CITY, EMAIL) \n" +
"                           VALUES(?,?,?,?,?,?,?,?,?)";
    public static final String QUERY_EMP = "SELECT * from EMPLOYEES";
    public static final String QUERY_EMP_DETAILS = "SELECT e FROM Employees e WHERE e.id =";
    public static final String QUERY_USERS = "SELECT * from CREDENTIALS";
    public static final String QUERY_DELETE = "DELETE from EMPLOYEES WHERE ID = ";
    public static final String MODIFY_EMP = "UPDATE Employees e SET e.name = :n, e.firstname = :fn, e.telhome = :th, e.telmob = :tm, e.telpro = :tp, e.adress = :a, e.postalcode = :pc, e.city= :c, e.email = :em WHERE e.id = :i";
}
