# ProjectJEE

Projet Java Enterprise Edition pour l'Efrei.

Group:
Nicolas PHILIPPE
Marie GALLARDO
Louis SINGER

Link to the remote depository:
https://gitlab.com/RicodeL/projectjee.git

Instructions:
The final version 2 with css is on the master branch.
Version 1 and Version 2 without css have their own branch.

No special instructions to launch the project on netbeans.

We used a database with adminDB for the user and password.

For the maven version, a pom.xml file was written. It can be used to compile
the project with Maven. You can use the keywords "clean package" to build it on jenkins.
