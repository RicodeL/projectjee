<%-- 
    Document   : newMember
    Created on : 11 déc. 2018, 10:58:48
    Author     : phili
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <link rel="stylesheet" href="Project_css.css">
        <c:set var = "employee" scope= "request" value = "${requestScope.employeeDetails}"/>
        
         <form name="newMember" action="Controller" method ="post">
             
            <div class="divNewMember">
                <div class="divNewMember2">
                    <h1>Member details page</h1>
                </div>
                <div style="padding-left: 25px">
                    <br/>
                    Name <input required type="text" value ="${employee.getName()}" name="name" class="inputNewMember"/>
                    <br />
                    Firstname <input required type="text" value ="${employee.getFirstname()}" name="firstName" class="inputNewMember"/>
                    <br />
                    tel.home <input required type="text" value ="${employee.getTelhome()}" name="telHome" class="inputNewMember"/>
                    <br />
                    tel.mobile <input required type="text" value ="${employee.getTelmob()}" name="telMob" class="inputNewMember"/>
                    <br />
                    tel.pro <input required type="text" value ="${employee.getTelpro()}" name="telPro" class="inputNewMember"/>
                    <br />
                    email <input required type="text" value ="${employee.getEmail()}" name="email" class="inputNewMember"/>
                    <br />
                    address <input required type="text" value ="${employee.getAddress()}" name="address" class="inputNewMember"/>
                    <br />
                    postal code <input required type="text" value ="${employee.getPostalcode()}" name="postalCode" class="inputNewMember"/>
                    <br />
                    city <input required type="text" value ="${employee.getCity()}" name="city"class="inputNewMember" />
                    <br />
                    <c:if test = "${employee.getName() != null}">

                    <input type="submit" name="saveEmployee" value="Save" class="submitButton"/>

                    </c:if>

                    <c:if test = "${employee.getName() == null}">

                    <input type="submit" name="addNewEmployee" value="Add" class="submitButton"/>

                    </c:if>
                </div>
            </div>
            
            
        </form>
            
        <form name="newMember" action="Controller" method ="post">
            <input type="submit" name="cancel" value="Cancel" />
        </form>
    </body>
</html>
