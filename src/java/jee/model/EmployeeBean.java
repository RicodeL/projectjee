/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jee.model;

/**
 *
 * @author phili
 */
public class EmployeeBean {
    private String name;
    private String firstname;
    private String telhome;
    private String telmob;
    private String telpro;
    private String address;
    private String postalcode;
    private String city;
    private String email;
    private int id;
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getTelhome() {
        return telhome;
    }

    public void setTelhome(String telhome) {
        this.telhome = telhome;
    }
    
    public String getTelmob() {
        return telmob;
    }
     public void setTelmob(String telmob) {
        this.telmob = telmob;
    }
    
    public void setTelpro(String telpro) {
        this.telpro = telpro;
    }
    public String getTelpro() {
        return telpro;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress (String address) {
        this.address = address;
    }
    
    public String getPostalcode() {
        return postalcode;
    }
    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }
    
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
}
