<%-- 
    Document   : login
    Created on : 26 oct. 2018, 15:38:24
    Author     : Jacques
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login page</title>
    </head>
    <body>
        <link rel="stylesheet" href="Project_css.css">
        <c:set var = "errorMsg" scope = "session" value = "${sessionScope.error}"/>
        <c:if test = "${errorMsg != null}">
            <p><font color="red"><c:out value = "${errorMsg}"/></font></p>
        </c:if>
        <c:set var="notfirstTime" value="true" scope="session"/>
        
        <form name="loginForm" action="Controller" method ="post">
            <br/>
            <div class="divForm">
                <div class="divLogin">
                    <a style="padding-left: 10px">
                        Login
                    </a>
                </div>
                <div style="padding-left: 25px">
                    <br/>
                    <input type="text" name="loginField" placeholder="Login" class="inputForm"/>
                    <br/>
                    <input type="text" name="pwdField" placeholder="Password" class="inputForm"/>
                    <br/>
                    <input type="submit" name="btnOK" value="Login" class="submitButton">
                    <br/>
                    <br/>
                </div>
            </div>
        </form>
    </body>
</html>
