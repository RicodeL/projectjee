/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jee.model;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import utils.Constants;
import static utils.Constants.*;
/**
 *
 * @author Jacques
 */
public class DataAccess {

    private Connection dbConn;
    private Statement stmt;
    private ResultSet rs;
    private String dbUrl;
    private String user;
    private String pwd;
    private ArrayList<EmployeeBean> employeesList;
    private ArrayList<UserBean> usersList;

    public Connection getConnection() throws IOException {
        
        Properties prop = new Properties();
            InputStream input;
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            input = cl.getResourceAsStream(Constants.PATH_PROPERTIES_FILE);
            prop.load(input);
            
            dbUrl = prop.getProperty(Constants.DB_URL);
            user = prop.getProperty(Constants.DB_USER);
            pwd = prop.getProperty(Constants.DB_PWD);
        try {

            dbConn = DriverManager.getConnection(dbUrl, user, pwd);

        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());

        }
        return dbConn;
    }

    public Statement getStatement(Connection dbConn) {
        try {
            stmt = dbConn.createStatement();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());

        }
        return stmt;

    }

    public ResultSet getResultSet(Statement stmt, String query) {

        try {
            rs = stmt.executeQuery(query);

        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());

        }
        return rs;

    }
    
    
    public void setResult(Statement stmt, String query) { //used for deletion

        try {
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public ArrayList getEmployees(ResultSet rs) {
        employeesList = new ArrayList<EmployeeBean>();
        try {
            while (rs.next()) {
                EmployeeBean employee = new EmployeeBean();
                employee.setId(rs.getInt("ID"));
                employee.setFirstname(rs.getString("FIRSTNAME"));
                employee.setName(rs.getString("NAME"));
                employee.setTelhome(rs.getString("TELHOME"));
                employee.setTelmob(rs.getString("TELMOB"));
                employee.setTelpro(rs.getString("TELPRO"));
                employee.setAddress(rs.getString("ADRESS"));
                employee.setPostalcode(rs.getString("POSTALCODE"));
                employee.setCity(rs.getString("CITY"));
                employee.setEmail(rs.getString("EMAIL"));
                employeesList.add(employee);
            }
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        return employeesList;
    }

    public String showEmployees(ArrayList<EmployeeBean> employeesList) {
           String output = "";
        for (EmployeeBean empl : employeesList) {
            output = output + empl.getFirstname() + "  -  "+empl.getName() + "<br/>";
        }
        
        return output;
    }
    
    public void deleteEmployee(String id) throws IOException { //not working
        String finalQuery = QUERY_DELETE + id;
        this.setResult(this.getStatement( this.getConnection()), finalQuery);
    }
    
    
        public ArrayList getUsers(ResultSet rs) {
        usersList = new ArrayList<UserBean>();
        try {
            while (rs.next()) {
                UserBean u = new UserBean();
                
                u.setLogin(rs.getString("LOGIN"));
                u.setPassword(rs.getString("PASSWORD"));
                usersList.add(u);
            }
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        return usersList;
    }
        
        
        
        public boolean addEmployee(EmployeeBean e) throws IOException{
        try{
            String querry = INSERT_EMPL;
            PreparedStatement pStat = getConnection().prepareStatement(querry);
            pStat.setString(1, e.getName());
            pStat.setString(2, e.getFirstname());
            pStat.setString(3, e.getTelhome());
            pStat.setString(4, e.getTelmob());
            pStat.setString(5, e.getTelpro());
            pStat.setString(6, e.getAddress());
            pStat.setString(7, e.getPostalcode());
            pStat.setString(8, e.getCity());
            pStat.setString(9, e.getEmail());
            pStat.executeUpdate();
        }catch(SQLException sqle){
            System.err.println(sqle.getMessage());
        }
        return true;
    }
        
        
        public boolean modifyEmployee(EmployeeBean e, String id) throws IOException{
        try{
            PreparedStatement pStat = getConnection().prepareStatement(MODIFY_EMP);
            pStat.setString(1, e.getName());
            pStat.setString(2, e.getFirstname());
            pStat.setString(3, e.getTelhome());
            pStat.setString(4, e.getTelmob());
            pStat.setString(5, e.getTelpro());
            pStat.setString(6, e.getAddress());
            pStat.setString(7, e.getPostalcode());
            pStat.setString(8, e.getCity());
            pStat.setString(9, e.getEmail());
            pStat.setString(10, id);
            pStat.executeUpdate();
        }catch(SQLException sqle){
            System.err.println(sqle.getMessage());
        }
        return true;
    }


}
