/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jee.model.*;
import static utils.Constants.*;

/**
 *
 * @author phili
 */
public class Controller extends HttpServlet {

    @EJB
    private EmployeeSessionBean employeesSessionBean;
    
    ArrayList<Employees> listEmployees;
    ArrayList<Credentials> listUsers;
    DataAccess db;
    String queryEmployees;
    String queryUser;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String loginEntered = request.getParameter("loginField");
            String pwdEntered = request.getParameter("pwdField");
            request.getSession().setAttribute("notfirstTime", null);
            db = new DataAccess();
            
            if(request.getSession().getAttribute("loggedInUser") == null && loginEntered == null) //rediretion to the login page
            {
                request.getSession().setAttribute("notfirstTime", true);
                request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
            }
            
            if (request.getParameter("log out") != null) // log out
            {
                LogOut(request, response);
            }
            
            if (request.getParameter("cancel") != null)
            {
                request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);   
            }
            
            
            if (request.getParameter("details") != null) // modification of an employee
            {
                GetDetailsEmpl(request, response);
            }
            
            if (request.getParameter("addNewEmployee") != null) // addition of a new employee
            {
                
                AddEmpl(request, response);
            }

            if (request.getParameter("add") != null) // addition of a new employee
            {
                request.getRequestDispatcher(NEWMEMBER).forward(request, response);
            }
            
            if (request.getParameter("saveEmployee") != null) // modification of a employee
            {
                SaveEmpl(request, response);
            }
            

            if (request.getParameter("delete") != null) // deletion of a employee
            {
                Delete_Empl(request, response);
            }
            
            if (request.getSession().getAttribute("notfirstTime") == null) { // if someone try to connect
                request.getSession().setAttribute("notfirstTime", true);
                if (!"".equals(loginEntered) && !"".equals(pwdEntered)) {
                    
                    Sign_in(request, loginEntered, pwdEntered, response);

                } else {
                    request.getSession().setAttribute("error", FIELDS_EMPTY);
                    request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
                }
            } else { // si c'est la première fois qu'un utilisateur arrive sur le site, le redirige vers le login
                request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
            }

        }
    }

    private void LogOut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("loggedInUser", null);
        request.getSession().invalidate();
        request.getRequestDispatcher(GOODBYE).forward(request, response);
    }

    private void GetDetailsEmpl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("employeeSelection") == null)
        {
            request.setAttribute("messageError", MSG_SELECT_EMPL);
            request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
        }
        
        listEmployees = new ArrayList<>();
        listEmployees.addAll(employeesSessionBean.getEmployeesWithID((String) request.getParameter("employeeSelection")));
        
        Employees emp = listEmployees.get(0);
        request.setAttribute("employeeDetails", emp);
        request.getSession().setAttribute("employeeID", emp.getId());
        request.getRequestDispatcher(NEWMEMBER).forward(request, response);
    }

    private void AddEmpl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EmployeeBean emp = new EmployeeBean();
        emp.setAddress(request.getParameter("address"));
        emp.setCity(request.getParameter("city"));
        emp.setEmail(request.getParameter("email"));
        emp.setFirstname(request.getParameter("firstName"));
        emp.setName(request.getParameter("name"));
        emp.setPostalcode(request.getParameter("postalCode"));
        emp.setTelhome(request.getParameter("telHome"));
        emp.setTelmob(request.getParameter("telMob"));
        emp.setTelpro(request.getParameter("telPro"));
        
        db.addEmployee(emp);
        
        listEmployees = new ArrayList<>();
        listEmployees.addAll(employeesSessionBean.getEmployees());
        
        request.getSession().setAttribute("keyEmplList", listEmployees);
        request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
    }

    private void SaveEmpl(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Employees emp = new Employees();
        emp.setAddress(request.getParameter("address"));
        emp.setCity(request.getParameter("city"));
        emp.setEmail(request.getParameter("email"));
        emp.setFirstname(request.getParameter("firstName"));
        emp.setName(request.getParameter("name"));
        emp.setPostalcode(request.getParameter("postalCode"));
        emp.setTelhome(request.getParameter("telHome"));
        emp.setTelmob(request.getParameter("telMob"));
        emp.setTelpro(request.getParameter("telPro"));
        emp.setId(Integer.parseInt(request.getSession().getAttribute("employeeID").toString()));
        //db.modifyEmployee(emp, id);
        emp = employeesSessionBean.modifyEmployee(emp);
        
        
        
        listEmployees = new ArrayList<>();
        listEmployees.addAll(employeesSessionBean.getEmployees());
        
        request.getSession().setAttribute("keyEmplList", listEmployees);
        request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
    }

    private void Delete_Empl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ID = request.getParameter("employeeSelection");
        
        if(ID == null)
        {
            request.setAttribute("messageError", MSG_SELECT_EMPL);
            request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
        }
        
        //db.deleteEmployee(ID);
        employeesSessionBean.deleteEmployee(Integer.parseInt(ID));
        
        listEmployees = new ArrayList<>();
        listEmployees.addAll(employeesSessionBean.getEmployees());
        
        isListEmpty(request); //set a message for welcomePage if the list is empty
        
        request.getSession().setAttribute("keyEmplList", listEmployees);
        request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
    }

    private void Sign_in(HttpServletRequest request, String loginEntered, String pwdEntered, HttpServletResponse response) throws ServletException, IOException {
        
        listEmployees = new ArrayList<>();
        listEmployees.addAll(employeesSessionBean.getEmployees());

        listUsers = new ArrayList<>();
        listUsers.addAll(employeesSessionBean.getUsers());
        
        isListEmpty(request);
        
        boolean found = false;
        for (Credentials u : listUsers) {
            if ((loginEntered.equals(u.getLogin())) && pwdEntered.equals(u.getPassword())) {
                found = true;
                request.getSession().setAttribute("keyEmplList", listEmployees);
                request.getSession().setAttribute("loggedInUser", loginEntered);
                request.getRequestDispatcher(WELCOME_PAGE).forward(request, response);
            }
        }
        if (!found) {
            request.getSession().setAttribute("error", WRONG_LOGIN);
            request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
        }
    }
    
    
    protected void isListEmpty(HttpServletRequest request)
    {
        if(listEmployees.isEmpty())
                    {
                        request.getSession().setAttribute("keyEmplListSize", NO_MEMBER);
                    }
                    else
                    {
                        request.getSession().setAttribute("keyEmplListSize", null);
                    }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
