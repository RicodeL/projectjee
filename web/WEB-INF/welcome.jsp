<%-- 
    Document   : welcome
    Created on : 24 nov. 2018, 10:26:04
    Author     : phili
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <link rel="stylesheet" href="Project_css.css">
        <%@ include file="header.jsp" %>
        <h1>Welcome and be overwhelmed by our employees!</h1>
        <c:set var = "messageError" scope= "request" value = "${requestScope.messageError}"/>
        <c:if test = "${messageError != null}">
                <p><font color = "blue"><b><c:out value="${messageError}"/></b></font></p>
        </c:if>
        
        
        <form name="loginForm" action="Controller">
        <table style="width:100%">
            <c:set var = "listEmpty" scope= "session" value = "${sessionScope.keyEmplListSize}"/>
            <c:if test = "${listEmpty != null}">
                <p><font color = "blue"><b><c:out value="${listEmpty}"/></b></font></p>
            </c:if>
            <c:if test = "${listEmpty == null}">
                <tr>
                <th>Select</th>
                <th>Firstname</th>
                <th>Name</th>
                <th>telhome</th>
                <th>telmob</th>
                <th>adress</th>
                <th>postalcode</th>
                <th>city</th>
                <th>email</th>
                </tr>
                <c:forEach items="${sessionScope.keyEmplList}" var = "empl">


                    <tr>
                    <td><input type="radio" name="employeeSelection" value="${empl.id}"></td>
                    <td><c:out value="${empl.name}"/></td>
                    <td><c:out value="${empl.firstname}"/></td>
                    <td><c:out value="${empl.telhome}"/></td>
                    <td><c:out value="${empl.telmob}"/></td>
                    <td><c:out value="${empl.address}"/></td>
                    <td><c:out value="${empl.postalcode}"/></td>
                    <td><c:out value="${empl.city}"/></td>
                    <td><c:out value="${empl.email}"/></td>
                    </tr>

                </c:forEach>
            
            
            
            <input type="submit" name="delete" value="DELETE" class="submitButton"/>
            <input type="submit" name="details" value="DETAILS" class="submitButton"/>
            </c:if>
            <input type="submit" name="add" value="ADD" class="submitButton"/>
        </table>
        </form>
            
        
    </body>
</html>
